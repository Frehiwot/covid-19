import 'package:flutter/material.dart';
import 'package:photo_view/photo_view.dart';
import './AppLanguage.dart';
import 'app_localizations.dart';
import 'package:provider/provider.dart';
class UseMask extends StatefulWidget{
  //  BuildContext context;
  //  WashingStep(this.context);
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _UseMaskState();
  }
}
class _UseMaskState extends State<UseMask>{
  @override
  Widget build(BuildContext context) {
    var appLanguage = Provider.of<AppLanguage>(context);
    // TODO: implement build
    return Scaffold(
      resizeToAvoidBottomInset: false,
      //resizeToAvoidBottomPadding: false,
      appBar:AppBar(
        automaticallyImplyLeading: true,
        leading: IconButton(
          icon: Icon(Icons.arrow_back), 
          onPressed: (){
            Navigator.pop(context);
          }),
        title: Text(AppLocalizations.of(context).translate('maskTitle')),),
      body: ListView (
        children: <Widget>[
          Text(AppLocalizations.of(context).translate('maskTitle2'),textAlign: TextAlign.center,style:TextStyle(fontWeight: FontWeight.bold,fontSize: 20,color:Colors.red)),
          Column(
            
            children:<Widget>[
              Container(
                margin:EdgeInsets.only(left: 10,top:12,right: 10,bottom: 0),
                child:Image.asset('assets/mask1.PNG')
                ),
                RichText(
                  textAlign: TextAlign.center,
                  text: TextSpan(
                    text: AppLocalizations.of(context).translate('maskStep1'),
                    style: TextStyle(fontSize: 15,fontStyle: FontStyle.normal,fontWeight: FontWeight.bold,color: Colors.black)
                    ),
                  )
            ]
          ),
          Column(
            children:<Widget>[
              Container(
                margin:EdgeInsets.only(left: 10,top:12,right: 10,bottom: 0),
                child:Image.asset('assets/mask2.PNG')
                ),
                RichText(
                  textAlign: TextAlign.center,
                  text: TextSpan(
                    text:AppLocalizations.of(context).translate('maskStep2'),
                    style: TextStyle(fontSize: 15,fontStyle: FontStyle.normal,fontWeight: FontWeight.bold,color: Colors.black)
                    ),
                  )
            ]
          ),
          Column(
            children:<Widget>[
              Container(
                margin:EdgeInsets.only(left: 10,top:12,right: 10,bottom: 0),
                child:Image.asset('assets/mask3.PNG')
                ),
                RichText(
                  textAlign: TextAlign.center,
                  text: TextSpan(
                    text:AppLocalizations.of(context).translate('maskStep3'),
                    style: TextStyle(fontSize: 15,fontStyle: FontStyle.normal,fontWeight: FontWeight.bold,color: Colors.black)
                    ),
                  )
            ]
          ),
          Column(
            children:<Widget>[
              Container(
                margin:EdgeInsets.only(left: 10,top:12,right: 10,bottom: 0),
                child:Image.asset('assets/mask4.PNG')
                ),
                RichText(
                  textAlign: TextAlign.center,
                  text: TextSpan(
                    text:AppLocalizations.of(context).translate('maskStep4'),
                    style: TextStyle(fontSize: 15,fontStyle: FontStyle.normal,fontWeight: FontWeight.bold,color: Colors.black)
                    ),
                  )
            ]
          ),
          Container(
            margin: EdgeInsets.all(15),
            child: Column(
              children:<Widget>[
                Text(AppLocalizations.of(context).translate('maskTitle3'),textAlign: TextAlign.center,style:TextStyle(fontWeight: FontWeight.bold,fontSize: 20,color: Colors.red)),
                Column(
                 children:<Widget>[
                  Container(
                    margin:EdgeInsets.only(left: 10,top:12,right: 10,bottom: 0),
                    child:Image.asset('assets/maskOff1.PNG')
                    ),
                    RichText(
                      textAlign: TextAlign.center,
                      text: TextSpan(
                        text:AppLocalizations.of(context).translate('unmaskStep1'),
                        style: TextStyle(fontSize: 15,fontStyle: FontStyle.normal,fontWeight: FontWeight.bold,color: Colors.black)
                        ),
                      )
                ]
              ),
              Column(
                children:<Widget>[
                  Container(
                    margin:EdgeInsets.only(left: 10,top:12,right: 10,bottom: 0),
                    child:Image.asset('assets/maskOff2.PNG')
                    ),
                    RichText(
                      textAlign: TextAlign.center,
                      text: TextSpan(
                        text:AppLocalizations.of(context).translate('unmaskStep2'),
                        style: TextStyle(fontSize: 15,fontStyle: FontStyle.normal,fontWeight: FontWeight.bold,color: Colors.black)
                        ),
                      )
                ]
              ),
               Column(
                children:<Widget>[
                  Container(
                    margin:EdgeInsets.only(left: 10,top:12,right: 10,bottom: 0),
                    child:Image.asset('assets/maskOff3.PNG')
                    ),
                    RichText(
                      textAlign: TextAlign.center,
                      text: TextSpan(
                        text:AppLocalizations.of(context).translate('unmaskStep3'),
                        style: TextStyle(fontSize: 15,fontStyle: FontStyle.normal,fontWeight: FontWeight.bold,color: Colors.black)
                        ),
                      )
                ]
              ),
              Column(
                children:<Widget>[
                  Container(
                    margin:EdgeInsets.only(left: 10,top:12,right: 10,bottom: 0),
                    child:Image.asset('assets/maskOff4.PNG')
                    ),
                    RichText(
                      textAlign: TextAlign.center,
                      text: TextSpan(
                        text:AppLocalizations.of(context).translate('unmaskStep4'),
                        style: TextStyle(fontSize: 15,fontStyle: FontStyle.normal,fontWeight: FontWeight.bold,color: Colors.black)
                        ),
                      )
                ]
              ),
               Column(
                children:<Widget>[
                  Container(
                    margin:EdgeInsets.only(left: 10,top:12,right: 10,bottom: 0),
                    child:Image.asset('assets/maskOff5.PNG')
                    ),
                    RichText(
                      textAlign: TextAlign.center,
                      text: TextSpan(
                        text:AppLocalizations.of(context).translate('unmaskStep5'),
                        
                        style: TextStyle( fontSize: 15,fontStyle: FontStyle.normal,fontWeight: FontWeight.bold,color: Colors.black)
                        ),
                      )
                ]
              ),
              ]
            ),)
        ],
      )
    // ),
    );
  }
  // Widget build(BuildContext context) {
    // TODO: implement build
    // return new WillPopScope(
    //   onWillPop: () async{
    //     // if(!_isOpened)
    //     // {
    //       // Navigator.pop(context);
    //       return new Future.value(true);
    //       // return Navigator.pop(context);
    //     //}
    //   },
    //   child: Scaffold(
    //     appBar: AppBar(
    //       automaticallyImplyLeading: true,
    //       leading: IconButton(
    //         icon: Icon(Icons.arrow_back), 
    //         onPressed: (){
    //           Navigator.pop(context,false);
    //         }),
    //       title: Text("corona"))
    //   )
      
    //   );
    // }
}