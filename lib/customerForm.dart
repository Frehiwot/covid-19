// import 'package:flutter/material.dart';
// import 'package:flutterproject/database_helpers.dart';

// void main() => runApp(MyApp());

// class MyApp extends StatelessWidget {
//   @override
//   Widget build(BuildContext context) {
//     final appTitle = 'Form Validation Demo';

//     return MaterialApp(
//       title: appTitle,
//       home: Scaffold(
//         appBar: AppBar(
//           title: Text(appTitle),
//         ),
//         body: MyCustomForm(),
//       ),
//     );
//   }
// }

// // Create a Form widget.
// class MyCustomForm extends StatefulWidget {
//   @override
//   MyCustomFormState createState() {
//     return MyCustomFormState();
//   }
// }

// // Create a corresponding State class.
// // This class holds data related to the form.
// class MyCustomFormState extends State<MyCustomForm> {
//   // Create a global key that uniquely identifies the Form widget
//   // and allows validation of the form.
//   //
//   // Note: This is a GlobalKey<FormState>,
//   // not a GlobalKey<MyCustomFormState>.
//   final Map<String,dynamic> formData={'email':null,'password':null};
//   final _formKey = GlobalKey<FormState>();

//   @override
//   Widget build(BuildContext context) {
//     // Build a Form widget using the _formKey created above.
//     return Form(
//       key: _formKey,
//       child: Column(
//         crossAxisAlignment: CrossAxisAlignment.start,
//         children: <Widget>[
//           TextFormField(
//             validator: (value) {
//               if (value.isEmpty) {
//                 return 'Please enter some text';
//               }
//               return null;
//             },
//             onSaved: (value){
//               formData['email']=value;
//             },
//           ),
//           TextFormField(
//             validator: (value){
//               if(value.isEmpty)
//               {
//                 return 'please enter some text';
//               }
//               return null;
//             },
//             onSaved: (value){
//               formData['password']=value;
//             },
//           ),
//           RaisedButton(
//             child:Text('read'),
//             onPressed: (){
//             _read();
//           }),
//           Padding(
//             padding: const EdgeInsets.symmetric(vertical: 16.0),
//             child: RaisedButton(
//               onPressed: () {
//                 // Validate returns true if the form is valid, or false
//                 // otherwise.
//                 if (_formKey.currentState.validate()) {
//                   // If the form is valid, display a Snackbar.
//                   _formKey.currentState.save();
//                   _save(formData);
//                   print("hello");
//                   Scaffold.of(context)
//                       .showSnackBar(SnackBar(content: Text('Processing Data')));
//                 }
//               },
//               child: Text('Submit'),
//             ),
//           ),
//         ],
//       ),
//     );
//   }
//   _save(formData) async{
//     // print("under save");
//     // print(formData);
//    Customer customer=new Customer();
//    customer.email=formData['email'];
//    customer.password=formData['password'];
//    DataHelpers db=DataHelpers.instance;

//    int id=await db.insert(customer);
//    print("id:-");
//    print(id);

//   }
//   _read() async{
//     DataHelpers db=DataHelpers.instance;
//     List<Customer> customer=await db.customers();
//     for(int i=0;i< customer.length;i++)
//     {
//       print("Customer:");
//       print(customer[i].email);
//       print(customer[i].password);
//     }
//     print(customer);
//   }
// }
//*************************************************************************************end of customer form with saving and validating capablity*********************************** */