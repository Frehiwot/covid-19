// import 'dart:io';
//     import 'package:path/path.dart';
//     import 'package:sqflite/sqflite.dart';
//     import 'package:path_provider/path_provider.dart';

//     // database table and column names
//     final String tableWords = 'words';
//     final String columnId = '_id';
//     final String columnWord = 'word';
//     final String columnFrequency = 'frequency';

//     // data model class
//     class Word {

//       int id;
//       String word;
//       int frequency;

//       Word();

//       // convenience constructor to create a Word object
//       Word.fromMap(Map<String, dynamic> map) {
//         id = map[columnId];
//         word = map[columnWord];
//         frequency = map[columnFrequency];
//       }

//       // convenience method to create a Map from this Word object
//       Map<String, dynamic> toMap() {
//         var map = <String, dynamic>{
//           columnWord: word,
//           columnFrequency: frequency
//         };
//         if (id != null) {
//           map[columnId] = id;
//         }
//         return map;
//       }
//     }

//     // singleton class to manage the database
//     class DatabaseHelper {

//       // This is the actual database filename that is saved in the docs directory.
//       static final _databaseName = "MyDatabase.db";
//       // Increment this version when you need to change the schema.
//       static final _databaseVersion = 1;

//       // Make this a singleton class.
//       DatabaseHelper._privateConstructor();
//       static final DatabaseHelper instance = DatabaseHelper._privateConstructor();

//       // Only allow a single open connection to the database.
//       static Database _database;
//       Future<Database> get database async {
//         if (_database != null) return _database;
//         _database = await _initDatabase();
//         return _database;
//       }

//       // open the database
//       _initDatabase() async {
//         // The path_provider plugin gets the right directory for Android or iOS.
//         Directory documentsDirectory = await getApplicationDocumentsDirectory();
//         String path = join(documentsDirectory.path, _databaseName);
//         // Open the database. Can also add an onUpdate callback parameter.
//         return await openDatabase(path,
//             version: _databaseVersion,
//             onCreate: _onCreate);
//       }

//       // SQL string to create the database 
//       Future _onCreate(Database db, int version) async {
//         await db.execute('''
//               CREATE TABLE $tableWords (
//                 $columnId INTEGER PRIMARY KEY,
//                 $columnWord TEXT NOT NULL,
//                 $columnFrequency INTEGER NOT NULL
//               )
//               ''');
//       }

//       // Database helper methods:

//       Future<int> insert(Word word) async {
//         Database db = await database;
//         int id = await db.insert(tableWords, word.toMap());
//         return id;
//       }

//       Future<Word> queryWord(int id) async {
//         Database db = await database;
//         List<Map> maps = await db.query(tableWords,
//             columns: [columnId, columnWord, columnFrequency],
//             where: '$columnId = ?',
//             whereArgs: [id]);
//         if (maps.length > 0) {
//           return Word.fromMap(maps.first);
//         }
//         return null;
//       }

//       // TODO: queryAllWords()
//       // TODO: delete(int id)
//       // TODO: update(Word word)
//     }

// import 'dart:io';
// import 'package:path/path.dart';
// import 'package:path_provider/path_provider.dart';
// import 'package:sqflite/sqflite.dart';
//    final String columnId = 'id';
//   final String columnAge = 'age';
//   final String columnName = 'name';
// class Dogs{
//   int id;
//   int age;
//   String name;

//   Dogs();
//   Dogs.fromMap(Map<String, dynamic> map) {
//         id = map[columnId];
//         age = map[columnAge];
//         name = map[columnName];
//       }

// //       // convenience method to create a Map from this Word object
//       Map<String, dynamic> toMap() {
//         var map = <String, dynamic>{
//           columnAge: age,
//           columnName: name
//         };
//         if (id != null) {
//           map[columnId] = id;
//         }
//         return map;
//       }
// //     }

//   // Map<String,dynamic> toMap(){
//   //   return {
//   //     'id':id,
//   //     'age':age,
//   //     'name':name
//   //   };
//   // }
// }
// class DatabaseHelpers{
//   static final _databaseName="dogs.db";
//   static final _databaseVersion=1;
//   static  Database _database;
//   DatabaseHelpers._privateConstructor();
//   static final DatabaseHelpers instance=DatabaseHelpers._privateConstructor();

//   Future<Database> get database async{
//     if(_database != null )
//     {
//       return _database;
//     }
//     _database=await _initDatabase();
//     return _database;
//   }
//   _initDatabase() async{
//    Directory documentsDirectory= await getApplicationDocumentsDirectory();
//    String path= join(documentsDirectory.path,_databaseName);
//    return await openDatabase(path,version: _databaseVersion,onCreate: _onCreate);
//   }

//   Future _onCreate(Database db,int version) async{
//    return db.execute('''CREATE TABLE DOGS(
//      id INTEGER PRIMARY KEY,
//      age INTEGER NOT NULL,
//      name TEXT NOT NULL
//    )''');

//   }
//   Future<int> insert(Dogs dog) async{
//    Database db=await database;
//    int id=await db.insert('DOGS',dog.toMap());
//    return id;
//   }


// }
import 'dart:io';
import 'package:flutter/material.dart';
import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';
import 'package:sqflite/sqflite.dart';

// final String columnEmail='email';
class Customer{
  String email;
  String password;
  int id;
  Customer({this.email,this.password});
  Map<String,dynamic> toMap()
  {
    return {
    'email':email,
    'password':password
    };
  }


}
class DataHelpers{
  final String _databaseName='customers.db';
  final int _databaseVersion=1;
 
  DataHelpers._privateConstructors();

  static final DataHelpers instance=DataHelpers._privateConstructors();
 static Database _database;
  Future<Database> get database async{
    if(_database != null){
      return _database;
    }
    _database=await _initDatabase();
     return _database;
  }

  _initDatabase() async{
    Directory documentsDirectory=await getApplicationDocumentsDirectory();
    String path=join(documentsDirectory.path,_databaseName);
    return await openDatabase(path,version: _databaseVersion,onCreate: _onCreate);
  }

  Future _onCreate(Database db,int version)
  {
    return db.execute('''CREATE TABLE customers (
      id INTEGER PRIMARY KEY,
      email TEXT ,
      password TEXT
    )''');
  }
  Future<int> insert(Customer customer) async{
    Database db=await database;
    int id=await db.insert('customers', customer.toMap());
    return id;
  }
  Future<List<Customer>> customers() async{
    Database db=await database;

    final List<Map<String,dynamic>> map=await db.query('customers');
    return List.generate(map.length, (i){
      return Customer(
        
        email:map[i]['email'],
        password:map[i]['password']
      );
    });
  }

  
  
  }