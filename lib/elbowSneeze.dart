
import 'package:flutter/material.dart';
import './AppLanguage.dart';
import 'app_localizations.dart';
import 'package:provider/provider.dart';
class ElbowSneeze extends StatelessWidget
{
  @override
  Widget build(BuildContext context) {
    var appLanguage = Provider.of<AppLanguage>(context);
    // TODO: implement build
    return Scaffold(
      
      appBar: AppBar(
        leading: IconButton(icon: Icon(Icons.arrow_back), onPressed: ()
        {
          Navigator.pop(context);
        }),
        title:Text(AppLocalizations.of(context).translate('elbowTitle'))
        ),
        body: ListView(
        children:<Widget>[
          Column(
        children:<Widget>[
          // Text(AppLocalizations.of(context).translate('elbowTitle2'),style:TextStyle(fontSize: 20,fontWeight: FontWeight.bold)),
          
          Container(
            margin: EdgeInsets.only(left:10,top:10,right:10,bottom:15),
          child:RichText(
            textAlign: TextAlign.center,
            text: TextSpan(
          text:AppLocalizations.of(context).translate('elbowMain'),
        
          style: TextStyle(fontSize: 15,fontStyle: FontStyle.normal,fontWeight: FontWeight.bold,color: Colors.black),

          ),
         
          ),
        ),
          Container(
            margin: EdgeInsets.only(left:15,top:5,right:15,bottom:10),
            child:Image.asset('assets/tooClose.PNG',)
          ),
          
        Column(
          children: <Widget>[
            Text(AppLocalizations.of(context).translate('elbowTitle3'),textAlign: TextAlign.center,style:TextStyle(fontSize: 15,fontWeight: FontWeight.bold)),
            Container(
             // margin: EdgeInsets.only(left:10,top:0,right:10,bottom:5),
              child: Image.asset('assets/elbowCover.PNG',width: 200,height: 200,),
              ),
            Container(
              margin: EdgeInsets.only(left:10,top:0,right:10,bottom:3),
              child:RichText(

              text: TextSpan(
                text:AppLocalizations.of(context).translate('elbowMain2'),
                style: TextStyle(fontSize: 15,fontStyle: FontStyle.normal,fontWeight: FontWeight.bold,color: Colors.black),

            ),
           textAlign: TextAlign.center,
         
          ),
        ),
          ]
        ),
         Column(
          children: <Widget>[
            // Text('If we keep our social distance'),
            Container(
              margin: EdgeInsets.only(left:10,top:10,right:10,bottom:5),
              child: Image.asset('assets/tissueCover.PNG',width: 200,height: 200,),
              ),
            Container(
              margin: EdgeInsets.only(left:10,top:0,right:10,bottom:10),
              child:RichText(
                textAlign:TextAlign.center,
                text: TextSpan(
              text:AppLocalizations.of(context).translate('elbowMain3'),
              style: TextStyle(fontSize: 15,fontStyle: FontStyle.normal,fontWeight: FontWeight.bold,color: Colors.black),
            ),
          
          ),
        ),
          ]
        )
        ]
        ),

        
        ]
        ),
    );
  }
}