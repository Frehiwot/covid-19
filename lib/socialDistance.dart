import 'package:flutter/material.dart';
import './AppLanguage.dart';
import 'app_localizations.dart';
import 'package:provider/provider.dart';

class SocialDistance extends StatelessWidget
{
  @override
  Widget build(BuildContext context) {
    var appLanguage = Provider.of<AppLanguage>(context);
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(icon: Icon(Icons.arrow_back), onPressed: ()
        {
          Navigator.pop(context);
        }),
        title:Text(AppLocalizations.of(context).translate('distanceTitle'))
        ),
        body: ListView(
        children:<Widget>[
          Column(
        children:<Widget>[
           Text(AppLocalizations.of(context).translate('distanceTitle2'),textAlign: TextAlign.center,style:TextStyle(fontSize: 20,fontWeight: FontWeight.bold)),
          Container(
            margin: EdgeInsets.only(left:10,top:15,right:10,bottom:5),
            child:Image.asset('assets/tooClose.PNG')
          ),
          Container(
            margin: EdgeInsets.only(left:15,top:15,right:15,bottom:15),
          child:RichText(
            textAlign: TextAlign.center,
            text: TextSpan(
          text:AppLocalizations.of(context).translate('closeDistance'),
        
          style: TextStyle(fontSize: 15,fontStyle: FontStyle.normal,fontWeight: FontWeight.bold,color: Colors.black),

          ),
          
          // textDirection: TextDirection.ltr,
          // softWrap: true,
          // overflow: TextOverflow.visible,
          // textScaleFactor: 1.0,
          ),
        ),
        Column(
          children: <Widget>[
            Text(AppLocalizations.of(context).translate('distanceTitle3')),
            Container(
              margin: EdgeInsets.only(left:10,top:0,right:10,bottom:5),
              child: Image.asset('assets/distancing.jpeg'),)
          ]
        )
        ]
        ),

        
        ]
        ),
    );
  }
}