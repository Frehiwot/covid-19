import 'package:flutter/material.dart';
import 'package:photo_view/photo_view.dart';
import './AppLanguage.dart';
import 'app_localizations.dart';
import 'package:provider/provider.dart';
class WashingStep extends StatefulWidget{
  //  BuildContext context;
  //  WashingStep(this.context);
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _WashingStepState();
  }
}
class _WashingStepState extends State<WashingStep>{
  @override
  Widget build(BuildContext context) {
    var appLanguage = Provider.of<AppLanguage>(context);
    // TODO: implement build
    return Scaffold(
      resizeToAvoidBottomInset: false,
      //resizeToAvoidBottomPadding: false,
      appBar:AppBar(
        automaticallyImplyLeading: true,
        leading: IconButton(
          icon: Icon(Icons.arrow_back), 
          onPressed: (){
            Navigator.pop(context);
          }),
        title: Text(AppLocalizations.of(context).translate('washingStepTitle')),),
      body:
      
      
      ListView (
        scrollDirection: Axis.vertical,
        shrinkWrap: true,
        children: <Widget>[
          // Text('hello'),
          Container(
             margin: EdgeInsets.only(top: 10,left: 10,right:10,bottom:10),
            //flex:1,
            child:Column(
            children:<Widget>[
            Container(
              color: Colors.white,
              //margin: const EdgeInsets.symmetric(vertical: 20.0),
              height: 100,
              width: 150,
             child:PhotoView(
               imageProvider: AssetImage('assets/washingStep1.PNG'),
               minScale: PhotoViewComputedScale.contained*0.8,
               maxScale: 9.0,
               basePosition:Alignment.center ,
              
            ) ,
            ),
            Container(
              child: Text(AppLocalizations.of(context).translate('washingStep1'),style:TextStyle(fontWeight:FontWeight.bold,fontSize:20))
              ,)
            ]
            )
            ,),

          Container(
            //flex:1,
            margin: EdgeInsets.only(top: 10,left: 10,right:10,bottom:30),
            child:Column(
            children:<Widget>[
            Container(
              color: Colors.white,
              //margin: const EdgeInsets.symmetric(vertical: 20.0),
              height: 100,
              width: 350,
             child:PhotoView(
               imageProvider: AssetImage('assets/washingStep2.PNG'),
               ///minScale: PhotoViewComputedScale.contained*1,
               backgroundDecoration:BoxDecoration(color: Colors.white),
               //maxScale: 9.0,
               basePosition:Alignment.center ,
             
            ) ,
            ),
            Container(
              child: Text(AppLocalizations.of(context).translate('washingStep2'),textAlign: TextAlign.center,style:TextStyle(fontWeight:FontWeight.bold,fontSize:20))
              ,)
            ]
            )
            ,),
            Container(
            //flex:1,
              margin: EdgeInsets.only(top: 10,left: 10,right:10,bottom:30),
              child:Column(
                children:<Widget>[
                  Container(
                    child:Image.asset('assets/washingStep3.PNG')
                  ) ,
                  Container(
                    child: Text(AppLocalizations.of(context).translate('washingStep3'),textAlign: TextAlign.center,style:TextStyle(fontWeight:FontWeight.bold,fontSize:20))
                    ,)
                ]
              )
            ,),
            Container(
            //flex:1,
              margin: EdgeInsets.only(top: 10,left: 10,right:10,bottom:30),
              child:Column(
                children:<Widget>[
                  Container(
                    child:Image.asset('assets/washingStep.PNG')
                  ) ,
                  Container(
                    child: Text(AppLocalizations.of(context).translate('washingStep4'),textAlign: TextAlign.center,style:TextStyle(fontWeight:FontWeight.bold,fontSize:20))
                    ,)
                ]
              )
            ,),
            Container(
            //flex:1,
             margin: EdgeInsets.only(top: 10,left: 10,right:10,bottom:30),
              child:Column(
                children:<Widget>[
                  Container(
                    child:Image.asset('assets/washingStep4.PNG')
                  ) ,
                  Container(
                    child: Text(AppLocalizations.of(context).translate('washingStep5'),textAlign: TextAlign.center,style:TextStyle(fontWeight:FontWeight.bold,fontSize:20))
                    ,)
                ]
             )
            ,),
            Container(
              margin: EdgeInsets.only(top: 10,left: 10,right:10,bottom:30),
            //flex:1,
              child:Column(
                children:<Widget>[
                  Container(
                    child:Image.asset('assets/washingStep5.PNG')
                  ) ,
                  Container(
                    child: Text(AppLocalizations.of(context).translate('washingStep6'),overflow: TextOverflow.visible,textAlign: TextAlign.center,style:TextStyle(fontWeight:FontWeight.bold,fontSize:20))
                    ,)
                ]
              )
            ,),
            
            Container(
              margin: EdgeInsets.only(top: 10,left: 10,right:10,bottom:30),
            //flex:1,
              child:Column(
                children:<Widget>[
                  Container(
                    child:Image.asset('assets/washingStep6.PNG')
                  ) ,
                  Container(
                    child: Text(AppLocalizations.of(context).translate('washingStep7'),overflow: TextOverflow.visible,textAlign: TextAlign.center,style:TextStyle(fontWeight:FontWeight.bold,fontSize:20))
                    ,)
                ]
              )
            ,),
             Container(
              margin: EdgeInsets.only(top: 10,left: 10,right:10,bottom:30),
            //flex:1,
              child:Column(
                children:<Widget>[
                  Container(
                    child:Image.asset('assets/washingStep7.PNG')
                  ) ,
                  Container(
                    child: Text(AppLocalizations.of(context).translate('washingStep8'),overflow: TextOverflow.visible,textAlign: TextAlign.center,style:TextStyle(fontWeight:FontWeight.bold,fontSize:20))
                    ,)
                ]
              )
            ,),
            Container(
              margin: EdgeInsets.only(top: 10,left: 10,right:10,bottom:30),
            //flex:1,
              child:Column(
                children:<Widget>[
                  Container(
                    child:Image.asset('assets/washingStep8.PNG')
                  ) ,
                  Container(
                    child: Text(AppLocalizations.of(context).translate('washingStep9'),overflow: TextOverflow.visible,textAlign: TextAlign.center,style:TextStyle(fontWeight:FontWeight.bold,fontSize:20))
                    ,)
                ]
              )
            ,),

          //   Flexible(
          //   flex:1,
          //   child:Column(
          //   children:<Widget>[
          //   Container(
          //     child:Image.asset('assets/washingStep7.PNG')
          //   ) ,
          //   Flexible(
          //     flex:1,
          //     child: Text('backs of fingers to opposing palms with fingers interlocked ',overflow: TextOverflow.visible,style:TextStyle(fontWeight:FontWeight.bold,fontSize:20))
          //     ,)
          //   ]
          //   )
          //   ,),
            

        ]
      )
    // ),
    );
  }
  // Widget build(BuildContext context) {
    // TODO: implement build
    // return new WillPopScope(
    //   onWillPop: () async{
    //     // if(!_isOpened)
    //     // {
    //       // Navigator.pop(context);
    //       return new Future.value(true);
    //       // return Navigator.pop(context);
    //     //}
    //   },
    //   child: Scaffold(
    //     appBar: AppBar(
    //       automaticallyImplyLeading: true,
    //       leading: IconButton(
    //         icon: Icon(Icons.arrow_back), 
    //         onPressed: (){
    //           Navigator.pop(context,false);
    //         }),
    //       title: Text("corona"))
    //   )
      
    //   );
    // }
}