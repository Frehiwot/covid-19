import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:flutterproject/elbowSneeze.dart';
import 'package:flutterproject/socialDistance.dart';
import 'package:flutterproject/useMask.dart';
import 'package:flutterproject/washingStep.dart';
import 'dart:async';
import 'dart:convert';
import 'package:flutter/services.dart';



// void main()=>runApp(MyApp());

// class MyApp extends StatefulWidget{
//   @override
//   State<StatefulWidget> createState() {
//     // TODO: implement createState
//     return _MyAppState();
//   }
// }
  
// class _MyAppState extends State<MyApp>{
//   @override
//   Widget build(BuildContext context) {
//     return MaterialApp(
//       debugShowCheckedModeBanner: false,
//       supportedLocales: [
//         Locale('en', 'US'),
//         Locale('am', ''),
//       ],
//       localizationsDelegates: [
//         AppLocalizations.delegate,
//         MaterialLocalizationAmDelegate(),
//         GlobalMaterialLocalizations.delegate,
//         GlobalWidgetsLocalizations.delegate,
//       ],
//       localeResolutionCallback: ( locale, supportedLocales)
//       {
//         print(supportedLocales.length);
//         for(var supportedLocaleLanguage in supportedLocales)
//         {
//           print("in for loop");
//           print(locale.languageCode);
//           print(locale.countryCode);
//           print('supported locale');
//           print(supportedLocaleLanguage.languageCode);
//           print(supportedLocaleLanguage.countryCode);
//           if(supportedLocaleLanguage.languageCode == locale.languageCode )
//           {
//             print('they are equal');
            
//               return supportedLocaleLanguage;
//           }
          
//         }
//         return supportedLocales.first;
//       },
//       home: HomeScreen()
//     );
//   }}

// class HomeScreen extends StatelessWidget{
// @override
//   Widget build(BuildContext context) {
//     // TODO: implement build
//     return 
//        Scaffold(
//         appBar:AppBar(title: Text(AppLocalizations.of(context).translate('title')),),
//         body:Column(
//           crossAxisAlignment:CrossAxisAlignment.start,
//           children: <Widget>[
           
               
//       Flexible(
//         flex:1,
//         child: InkWell(
//           child:new Container(
//           margin: EdgeInsets.only(top: 10,left: 10,right:10,bottom:10),
//           color: Colors.grey,
//           child: Row(
//             children: <Widget>[
//               Container(
//                 margin: EdgeInsets.only(top: 10,left: 10,right:10,bottom:10),
//                //constraints: BoxConstraints.expand(height:50.0),
//                 child: Image.asset('assets/socialDistancing1.jpeg',height: 100,width: 100)
//               ),
//                Container(
//                // margin: EdgeInsets.only(top: 10,left: 10,right:10,bottom:10),
//                //constraints: BoxConstraints.expand(height:50.0),
//                child: Text('Keep your social distance',overflow: TextOverflow.visible,style: TextStyle(fontSize: 15,fontWeight: FontWeight.bold),)
                
//               ),

//             ],
//             ),
          
//           ),
//           onTap:()
//           {
//             Navigator.push(context, MaterialPageRoute(builder: (BuildContext context)=> SocialDistance()));
//           }
//           ),
//       ),
//        Flexible(
//         flex:1,
//         child: InkWell(
//           child:Container(
//           margin: EdgeInsets.only(top: 10,left: 10,right:10,bottom:10),
//           color: Colors.grey,
//           child: Row(
//             children:<Widget>[
//               Container(
//                 margin: EdgeInsets.only(top: 10,left: 10,right:10,bottom:10),
//                //constraints: BoxConstraints.expand(height:50.0),
//                 child: Image.asset('assets/handWashing.PNG',height: 100,width: 100,)
//               ),
//                Container(
//                // margin: EdgeInsets.only(top: 10,left: 10,right:10,bottom:10),
//                //constraints: BoxConstraints.expand(height:50.0),
//                child: Text('10 steps to wash ur hand',overflow: TextOverflow.visible,style: TextStyle(fontSize: 15,fontWeight: FontWeight.bold),)
                
//               ),
              
//             ]
           
//           ),
//           ),
//           onTap: ()
//           {
//             print("tapping cotainer");
//             Navigator.push(context, MaterialPageRoute(builder: (BuildContext context)=> WashingStep()));
//           },
//         ),
//         ) ,
//           Flexible(
//         flex:1,
//         child: InkWell(
//           child:new Container(
//           margin: EdgeInsets.only(top: 10,left: 10,right:10,bottom:10),
          
//           color: Colors.grey,
//           child:Row(
//             children: <Widget>[
//               Container(
//                 margin: EdgeInsets.only(top: 10,left: 10,right:10,bottom:10),
//                //constraints: BoxConstraints.expand(height:50.0),
//                 child: Image.asset('assets/elbow2.jpeg',height: 100,width: 80,)
//               ),
//                Container(
//                // margin: EdgeInsets.only(top: 10,left: 10,right:10,bottom:10),
//                //constraints: BoxConstraints.expand(height:50.0),
//                child: Text('Use your elbow when you sneeze',overflow: TextOverflow.visible,style: TextStyle(fontSize: 15,fontWeight: FontWeight.bold),)
                
//               ),
//             ]
//             ,)
//           ),
//           onTap: ()
//           {
//             Navigator.push(context, MaterialPageRoute(builder: (BuildContext context)=> ElbowSneeze()));
//           },
//         ),
//           ),
//         Flexible(
//         flex:1,
//         child: InkWell(
//           child:new Container(
//           margin: EdgeInsets.only(top: 10,left: 10,right:10,bottom:10),
          
//           color: Colors.grey,
//           child:Row(
//             children: <Widget>[
//               Container(
//                 margin: EdgeInsets.only(top: 10,left: 0,right:0,bottom:10),
//                //constraints: BoxConstraints.expand(height:50.0),
//                 child: Image.asset('assets/mask.png',height: 100,width: 100,)
//               ),
//                Container(
//                // margin: EdgeInsets.only(top: 10,left: 10,right:10,bottom:10),
//                //constraints: BoxConstraints.expand(height:50.0),
//                child: Text('Use mask and glooves properly',overflow: TextOverflow.visible,style: TextStyle(fontSize: 15,fontWeight: FontWeight.bold),)
                
//               ),
//             ]
//             ,)
//           ),
//           onTap: ()
//           {
//             Navigator.push(context, MaterialPageRoute(builder: (BuildContext context)=> UseMask()));
//           },
//         )
//           )   
//       ]
//         ),
//       );

//     //);
//   }
// }



//   class AppLang extends StatefulWidget {
//     @override
//   State<StatefulWidget> createState() {
//     // TODO: implement createState
//     return _AppLangState();
//   }
//   }
// class _AppLangState extends State<AppLang>{
//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       appBar: AppBar(),
//       body: Center(
//         child: Text(AppLocalizations.of(context).translate('Message')),
//       ),
//     );
//   }
// }

// class _AppLocalizationsDelegate
//     extends LocalizationsDelegate<AppLocalizations> {
//   // This delegate instance will never change (it doesn't even have fields!)
//   // It can provide a constant constructor.
//   const _AppLocalizationsDelegate();

//   @override
//   bool isSupported(Locale locale) {
//     // Include all of your supported language codes here
//     return ['en', 'am'].contains(locale.languageCode);
//   }

//   @override
//   Future<AppLocalizations> load(Locale locale) async {
//     // AppLocalizations class is where the JSON loading actually runs
//     AppLocalizations localizations = new AppLocalizations(locale);
//     await localizations.load();
//     return localizations;
//   }

//   @override
//   bool shouldReload(_AppLocalizationsDelegate old) => false;
// }

// class AppLocalizations {
//   final Locale locale;

//   AppLocalizations(this.locale);

//   // Helper method to keep the code in the widgets concise
//   // Localizations are accessed using an InheritedWidget "of" syntax
//   static AppLocalizations of(BuildContext context) {
//     return Localizations.of<AppLocalizations>(context, AppLocalizations);
//   }

//   // Static member to have a simple access to the delegate from the MaterialApp
//   static const LocalizationsDelegate<AppLocalizations> delegate =
//   _AppLocalizationsDelegate();

//   Map<String, String> _localizedStrings;

//   Future<bool> load() async {
//     // Load the language JSON file from the "lang" folder
//     String jsonString =
//     await rootBundle.loadString('i18n/${locale.languageCode}.json');
//     Map<String, dynamic> jsonMap = json.decode(jsonString);

//     _localizedStrings = jsonMap.map((key, value) {
//       return MapEntry(key, value.toString());
//     });

//     return true;
//   }

//   // This method will be called from every widget which needs a localized text
//   String translate(String key) {
//     return _localizedStrings[key];
//   }
// }

///// end of it///////////////////////////////////////////////////////////////////
///
///



import './AppLanguage.dart';
import 'app_localizations.dart';
import 'package:provider/provider.dart';

void main() async {
  AppLanguage appLanguage = AppLanguage();
  await appLanguage.fetchLocale();
  runApp(MyApp(
    appLanguage: appLanguage,
  ));
}

class MyApp extends StatelessWidget {
  final AppLanguage appLanguage;

  MyApp({this.appLanguage});

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider<AppLanguage>(
      create: (_) => appLanguage,
      child: Consumer<AppLanguage>(builder: (context, model, child) {
        return MaterialApp(
          locale: model.appLocal,
          supportedLocales: [
            Locale('en', 'US'),
            Locale('am', ''),
          ],
          localizationsDelegates: [
            AppLocalizations.delegate,
            MaterialLocalizationAmDelegate(),
            GlobalMaterialLocalizations.delegate,
            GlobalWidgetsLocalizations.delegate,
          ],
          localeResolutionCallback: ( locale, supportedLocales)
          {
            print(supportedLocales.length);
            for(var supportedLocaleLanguage in supportedLocales)
            {
              print("in for loop");
              print(locale.languageCode);
              print(locale.countryCode);
              print('supported locale');
              print(supportedLocaleLanguage.languageCode);
              print(supportedLocaleLanguage.countryCode);
              if(supportedLocaleLanguage.languageCode == locale.languageCode )
              {
                print('they are equal');
                
                  return supportedLocaleLanguage;
              }
              
            }
            return supportedLocales.first;
          },

          home: AppLang(),
        );
      }), 
    );
  }
}

class AppLang extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var appLanguage = Provider.of<AppLanguage>(context);
    return Scaffold(
      drawer: Drawer(
        
         //to create the slide bar on the left side
        // child: Column(
          
        //   children: <Widget>[
        //     AppBar(
        //       automaticallyImplyLeading: false, // to be able to to slide the slide bar
        //       title: Text("Choose"),
        //     ),
        //     RaisedButton(
        //       onPressed: () {
        //         appLanguage.changeLanguage(Locale("en"));
        //       },
        //       child: Text('English'),
        //     ),
        //     RaisedButton(
        //       onPressed: () {
        //         appLanguage.changeLanguage(Locale("am"));
        //       },
        //       child: Text('አማርኛ'),
        //     ),
        child: ListView(
          padding: EdgeInsets.zero,
          children: <Widget>[
            // DrawerHeader(
            //   child: Text('Choose',style: TextStyle(fontSize: 12,fontWeight: FontWeight.bold,color: Colors.white)),
            //   decoration: BoxDecoration(
            //     color: Colors.blue
            //   ),
            // ),
            AppBar(
              automaticallyImplyLeading: false, // to be able to to slide the slide bar
              title: Text('Choose',style: TextStyle(fontWeight: FontWeight.bold,color: Colors.white)),
            ),
            ListTile(
              title: Text('English'),
              onTap:(){
                appLanguage.changeLanguage(Locale("en"));
                Navigator.pop(context);
              }
            ),
            ListTile(
              title: Text('Amharic'),
              onTap:(){
                appLanguage.changeLanguage(Locale("am"));
                Navigator.pop(context);
              }
            ),
           
          ],
        ),
      ),
      appBar: AppBar(
        title: Text(AppLocalizations.of(context).translate('title')),
        
      ),
      body: Column(
        crossAxisAlignment:CrossAxisAlignment.start,
        
        children: <Widget>[
          
        Flexible(
        flex:1,
        child: InkWell(
          child:new Container(
          margin: EdgeInsets.only(top: 10,left: 10,right:10,bottom:10),
          color: Colors.grey,
          child: Row(
            children: <Widget>[
              Container(
                margin: EdgeInsets.only(top: 10,left: 10,right:10,bottom:10),
               //constraints: BoxConstraints.expand(height:50.0),
                child: Image.asset('assets/socialDistancing1.jpeg',height: 100,width: 100)
              ),
               Container(
               // margin: EdgeInsets.only(top: 10,left: 10,right:10,bottom:10),
               //constraints: BoxConstraints.expand(height:50.0),
               child: Text(AppLocalizations.of(context).translate('distance'),overflow: TextOverflow.visible,style: TextStyle(fontSize: 15,fontWeight: FontWeight.bold),)
                
              ),

            ],
            ),
          
          ),
          onTap:()
          {
            Navigator.push(context, MaterialPageRoute(builder: (BuildContext context)=> SocialDistance()));
          }
          ),
      ),
       Flexible(
        flex:1,
        child: InkWell(
          child:Container(
          margin: EdgeInsets.only(top: 10,left: 10,right:10,bottom:10),
          color: Colors.grey,
          child: Row(
            children:<Widget>[
              Container(
                margin: EdgeInsets.only(top: 10,left: 10,right:10,bottom:10),
               //constraints: BoxConstraints.expand(height:50.0),
                child: Image.asset('assets/handWashing.PNG',height: 100,width: 100,)
              ),
               Container(
               // margin: EdgeInsets.only(top: 10,left: 10,right:10,bottom:10),
               //constraints: BoxConstraints.expand(height:50.0),
               child: Text(AppLocalizations.of(context).translate('wash'),overflow: TextOverflow.visible,style: TextStyle(fontSize: 15,fontWeight: FontWeight.bold),)
                
              ),
              
            ]
           
          ),
          ),
          onTap: ()
          {
            print("tapping cotainer");
            Navigator.push(context, MaterialPageRoute(builder: (BuildContext context)=> WashingStep()));
          },
        ),
        ) ,
        Flexible(
        flex:1,
        child: InkWell(
          child:new Container(
          margin: EdgeInsets.only(top: 10,left: 10,right:10,bottom:10),
          
          color: Colors.grey,
          child:Row(
            children: <Widget>[
              Container(
                margin: EdgeInsets.only(top: 10,left: 10,right:10,bottom:10),
               //constraints: BoxConstraints.expand(height:50.0),
                child: Image.asset('assets/elbow2.jpeg',height: 100,width: 80,)
              ),
               Container(
               // margin: EdgeInsets.only(top: 10,left: 10,right:10,bottom:10),
               //constraints: BoxConstraints.expand(height:50.0),
               child: Text(AppLocalizations.of(context).translate('elbow'),overflow: TextOverflow.visible,style: TextStyle(fontSize: 15,fontWeight: FontWeight.bold),)
                
              ),
            ]
            ,)
          ),
          onTap: ()
          {
            Navigator.push(context, MaterialPageRoute(builder: (BuildContext context)=> ElbowSneeze()));
          },
        ),
          ),
        Flexible(
        flex:1,
        child: InkWell(
          child:new Container(
          margin: EdgeInsets.only(top: 10,left: 10,right:10,bottom:10),
          
          color: Colors.grey,
          child:Row(
            children: <Widget>[
              Container(
                margin: EdgeInsets.only(top: 10,left: 0,right:0,bottom:10),
               //constraints: BoxConstraints.expand(height:50.0),
                child: Image.asset('assets/mask.png',height: 100,width: 100,)
              ),
               Container(
               // margin: EdgeInsets.only(top: 10,left: 10,right:10,bottom:10),
               //constraints: BoxConstraints.expand(height:50.0),
               child: Text(AppLocalizations.of(context).translate('mask'),overflow: TextOverflow.visible,style: TextStyle(fontSize: 15,fontWeight: FontWeight.bold),)
                
              ),
            ]
            ,)
          ),
          onTap: ()
          {
            Navigator.push(context, MaterialPageRoute(builder: (BuildContext context)=> UseMask()));
          },
        )
          )   
      ]
        ),
      

    //);
      );
  
  }
}
////
  class MaterialLocalizationAmDelegate extends LocalizationsDelegate<MaterialLocalizations>{
    @override
  bool isSupported(Locale locale)=> locale.languageCode =="am";

  @override
  Future<MaterialLocalizations> load(Locale locale) async => MaterialLocalizationAm();

  @override
  bool shouldReload(_) {
    // TODO: implement shouldReload
    return false;
  }
  
  }
  class MaterialLocalizationAm extends MaterialLocalizations{
  @override
  String aboutListTileTitle(String applicationName) {
    // TODO: implement aboutListTileTitle
    return null;
  }

  @override
  // TODO: implement alertDialogLabel
  String get alertDialogLabel => null;

  @override
  // TODO: implement anteMeridiemAbbreviation
  String get anteMeridiemAbbreviation => null;

  @override
  // TODO: implement backButtonTooltip
  String get backButtonTooltip => null;

  @override
  // TODO: implement cancelButtonLabel
  String get cancelButtonLabel => null;

  @override
  // TODO: implement closeButtonLabel
  String get closeButtonLabel => null;

  @override
  // TODO: implement closeButtonTooltip
  String get closeButtonTooltip => null;

  @override
  // TODO: implement continueButtonLabel
  String get continueButtonLabel => null;

  @override
  // TODO: implement copyButtonLabel
  String get copyButtonLabel => null;

  @override
  // TODO: implement cutButtonLabel
  String get cutButtonLabel => null;

  @override
  // TODO: implement deleteButtonTooltip
  String get deleteButtonTooltip => null;

  @override
  // TODO: implement dialogLabel
  String get dialogLabel => null;

  @override
  // TODO: implement drawerLabel
  String get drawerLabel => null;

  @override
  // TODO: implement firstDayOfWeekIndex
  int get firstDayOfWeekIndex => null;

  @override
  String formatDecimal(int number) {
    // TODO: implement formatDecimal
    return null;
  }

  @override
  String formatFullDate(DateTime date) {
    // TODO: implement formatFullDate
    return null;
  }

  @override
  String formatHour(TimeOfDay timeOfDay, {bool alwaysUse24HourFormat = false}) {
    // TODO: implement formatHour
    return null;
  }

  @override
  String formatMediumDate(DateTime date) {
    // TODO: implement formatMediumDate
    return null;
  }

  @override
  String formatMinute(TimeOfDay timeOfDay) {
    // TODO: implement formatMinute
    return null;
  }

  @override
  String formatMonthYear(DateTime date) {
    // TODO: implement formatMonthYear
    return null;
  }

  @override
  String formatTimeOfDay(TimeOfDay timeOfDay, {bool alwaysUse24HourFormat = false}) {
    // TODO: implement formatTimeOfDay
    return null;
  }

  @override
  String formatYear(DateTime date) {
    // TODO: implement formatYear
    return null;
  }

  @override
  // TODO: implement hideAccountsLabel
  String get hideAccountsLabel => null;

  @override
  // TODO: implement licensesPageTitle
  String get licensesPageTitle => null;

  @override
  // TODO: implement modalBarrierDismissLabel
  String get modalBarrierDismissLabel => null;

  @override
  // TODO: implement narrowWeekdays
  List<String> get narrowWeekdays => null;

  @override
  // TODO: implement nextMonthTooltip
  String get nextMonthTooltip => null;

  @override
  // TODO: implement nextPageTooltip
  String get nextPageTooltip => null;

  @override
  // TODO: implement okButtonLabel
  String get okButtonLabel => null;

  @override
  // TODO: implement openAppDrawerTooltip
  String get openAppDrawerTooltip => null;

  @override
  String pageRowsInfoTitle(int firstRow, int lastRow, int rowCount, bool rowCountIsApproximate) {
    // TODO: implement pageRowsInfoTitle
    return null;
  }

  @override
  // TODO: implement pasteButtonLabel
  String get pasteButtonLabel => null;

  @override
  // TODO: implement popupMenuLabel
  String get popupMenuLabel => null;

  @override
  // TODO: implement postMeridiemAbbreviation
  String get postMeridiemAbbreviation => null;

  @override
  // TODO: implement previousMonthTooltip
  String get previousMonthTooltip => null;

  @override
  // TODO: implement previousPageTooltip
  String get previousPageTooltip => null;

  @override
  // TODO: implement refreshIndicatorSemanticLabel
  String get refreshIndicatorSemanticLabel => null;

  @override
  String remainingTextFieldCharacterCount(int remaining) {
    // TODO: implement remainingTextFieldCharacterCount
    return null;
  }

  @override
  // TODO: implement reorderItemDown
  String get reorderItemDown => null;

  @override
  // TODO: implement reorderItemLeft
  String get reorderItemLeft => null;

  @override
  // TODO: implement reorderItemRight
  String get reorderItemRight => null;

  @override
  // TODO: implement reorderItemToEnd
  String get reorderItemToEnd => null;

  @override
  // TODO: implement reorderItemToStart
  String get reorderItemToStart => null;

  @override
  // TODO: implement reorderItemUp
  String get reorderItemUp => null;

  @override
  // TODO: implement rowsPerPageTitle
  String get rowsPerPageTitle => null;

  @override
  // TODO: implement scriptCategory
  ScriptCategory get scriptCategory => null;

  @override
  // TODO: implement searchFieldLabel
  String get searchFieldLabel => null;

  @override
  // TODO: implement selectAllButtonLabel
  String get selectAllButtonLabel => null;

  @override
  String selectedRowCountTitle(int selectedRowCount) {
    // TODO: implement selectedRowCountTitle
    return null;
  }

  @override
  // TODO: implement showAccountsLabel
  String get showAccountsLabel => null;

  @override
  // TODO: implement showMenuTooltip
  String get showMenuTooltip => null;

  @override
  // TODO: implement signedInLabel
  String get signedInLabel => null;

  @override
  String tabLabel({int tabIndex, int tabCount}) {
    // TODO: implement tabLabel
    return null;
  }

  @override
  TimeOfDayFormat timeOfDayFormat({bool alwaysUse24HourFormat = false}) {
    // TODO: implement timeOfDayFormat
    return null;
  }

  @override
  // TODO: implement timePickerHourModeAnnouncement
  String get timePickerHourModeAnnouncement => null;

  @override
  // TODO: implement timePickerMinuteModeAnnouncement
  String get timePickerMinuteModeAnnouncement => null;

  @override
  // TODO: implement viewLicensesButtonLabel
  String get viewLicensesButtonLabel => null;
    
  }
  